package it.unibo.oop.lab.reactivegui03;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;


public class AnotherConcurrentGUI {
	
	private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
	final private JLabel text = new JLabel();

	public AnotherConcurrentGUI() {
		final Agent agent = new Agent();
		final JFrame frame = new JFrame();
		final JPanel panel = new JPanel();
		new Thread(agent).start(); 
		final JButton up = new JButton("up");
		up.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				agent.upCounting();
			}
		});
		
		final JButton down = new JButton("down");
		down.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				agent.downCounting();
			}
		});
		
		final JButton stop = new JButton("stop");
		stop.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				agent.stopCounting();
			}
			
		});

		frame.add(panel);
		panel.add(up);
		panel.add(down);
		panel.add(stop);
		panel.add(text);
		
		
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        frame.setVisible(true);
	}
	
	protected class Agent implements Runnable {
		
		private volatile boolean stop;
		private volatile boolean upDown;
		private int counter;

		@Override
		public void run() {
			while(!this.stop) {
				try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            AnotherConcurrentGUI.this.text.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                    
                    if(upDown) {
                    	this.counter++;
                    } else {
                    	this.counter--;
                    }          
                    
                    Thread.sleep(100);
                    
                } catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
			}
			
		}
		
		public void stopCounting() {
			this.stop = true;
		}
		
		public void upCounting() {
			this.upDown = true;
		}
		
		public void downCounting() {
			this.upDown = false;
		}
		
	}
	
	private class stopAgent extends Thread {
		
		 private static final long DEADLINE = 10000;

	        public void run() {
	            try {
	                Thread.sleep(DEADLINE);
	            } catch (Exception ex) {
	             // interrupted: added a system.out but there are much better ways to log exceptions
	                System.out.println("Something went wrong. " + ex);
	            }
	            AnotherConcurrentGUI.stopCounting();
	        }
	}

}
